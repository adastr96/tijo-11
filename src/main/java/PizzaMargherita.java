public class PizzaMargherita extends Pizza {

    String[] cheeseUsed = {"Mozarella", "Chedar"};
    String[] herbsUsed = {"Italian herbs"};
    String[] condimentsUsed = {"Oil", "Vinegar"};

    boolean pizzaContainsMeat(){return false;}
    boolean pizzaContainsVegetables(){return false;}

    void addMeat(){}

    void addCheese(){
        System.out.println("Dodanie sera: ");
        for(String cheese : cheeseUsed){
            System.out.print(cheese + " ");
        }
        System.out.print("\n");
    }

    void addHerbs(){
        System.out.println("Dodanie ziół: ");
        for(String herbs : herbsUsed){
            System.out.print(herbs + " ");
        }
        System.out.print("\n");
    }

    void addVegetables(){}

    void addCondiments() {
        System.out.println("Dodanie przypraw: ");
        for(String condiments : condimentsUsed){
            System.out.print(condiments + " ");
        }
        System.out.print("\n");
    }
}
