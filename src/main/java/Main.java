public class Main {
    public static void main(String[] args){
        Pizza prosciutto = new PizzaProsciutto();
        Pizza margherita = new PizzaMargherita();

        prosciutto.makePizza();
        margherita.makePizza();
    }
}
