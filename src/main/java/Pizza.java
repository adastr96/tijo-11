public abstract class Pizza {
    final void makePizza(){
        addTomatoeSauce();
        if(pizzaContainsMeat()){
            addMeat();
        }
        if(pizzaContainsCheese()){
            addCheese();
        }
        if(pizzaContainsHerbs()){
            addHerbs();
        }
        if(pizzaContainsVegetables()){
            addVegetables();
        }
        if(pizzaContainsCondiments()){
            addCondiments();
        }
        putInBake();
    }

    public void addTomatoeSauce(){
        System.out.println("dodawanie sosu pomidorowego");
    }
    public void putInBake(){
        System.out.println("umieszczanie pizzy w piecu \n");
    }

    abstract void addMeat();
    abstract void addCheese();
    abstract void addHerbs();
    abstract void addVegetables();
    abstract void addCondiments();

    boolean pizzaContainsMeat(){return true;}
    boolean pizzaContainsCheese(){return true;}
    boolean pizzaContainsHerbs(){return true;}
    boolean pizzaContainsVegetables(){return true;}
    boolean pizzaContainsCondiments(){return true;}
}
