public class PizzaProsciutto extends Pizza {

    String[] meatUsed = {"Salami", "Pepperoni", "Copicola Ham"};
    String[] cheeseUsed = {"Provolone", "Mozarella"};
    String[] herbsUsed = {"Italian herbs"};
    String[] veggiesUsed = {"Lettuce", "Tomatoes", "Onions", "Sweet peppers"};
    String[] condimentsUsed = {"Oil", "Vinegar"};

    void addMeat() {
        System.out.println("Dodanie mięsa: ");
        for(String meat : meatUsed){
            System.out.print(meat + " ");
        }
        System.out.print("\n");
    }

    void addCheese() {
        System.out.println("Dodanie sera: ");
        for(String cheese : cheeseUsed){
            System.out.print(cheese + " ");
        }
        System.out.print("\n");
    }

    void addHerbs(){
        System.out.println("Dodanie ziół: ");
        for(String herbs : herbsUsed){
            System.out.print(herbs + " ");
        }
        System.out.print("\n");
    }

    void addVegetables() {
        System.out.println("Dodanie warzyw: ");
        for(String veggies : veggiesUsed){
            System.out.print(veggies + " ");
        }
        System.out.print("\n");
    }

    void addCondiments() {
        System.out.println("Dodanie przypraw: ");
        for(String condiments : condimentsUsed){
            System.out.print(condiments + " ");
        }
        System.out.print("\n");
    }
}
